/*
Given a sequence of integers as an array, determine whether it is possible to obtain 
a strictly increasing sequence by removing no more than one element from the array.

Example

For sequence = [1, 3, 2, 1], the output should be
almostIncreasingSequence(sequence) = false;

There is no one element in this array that can be removed in order to get a strictly 
increasing sequence.

For sequence = [1, 3, 2], the output should be
almostIncreasingSequence(sequence) = true.

You can remove 3 from the array to get the strictly increasing sequence [1, 2]. 
Alternately, you can remove 2 to get the strictly increasing sequence [1, 3].
*/

bool almostIncreasingSequence(std::vector<int> sequence) {

    int erasedElements = 0;
    for (int i = 0, j = 1; j < sequence.size(); j++) {
        if(sequence[i] >= sequence[j])
        {
            erasedElements += 1;
            if (j > 1)
            {
                continue;
            }
        }
        i = j;
    }
    
    cout << erasedElements;
    
    return (erasedElements <= 1);
}
    
/*
    int i = 0;
    bool initialized = false;
    
    for(std::vector<int>::const_iterator i = sequence.begin(); i != sequence.end(); i++)
        cout << *i << ' ';
    
    for(i = 0; i < sequence.size() - 1; i++) {
        if(sequence[i] < sequence[i+1]) {
            if(i == sequence.size() - 1) {
                return true;
            }
        }
        if((sequence[i] == sequence[i+1]) && (sequence.size() == 2)) {
            return true;
        }
        if(!initialized) {
            if(sequence[i] >= sequence[i+1]) {
                sequence.erase(sequence.begin()+i);
                initialized = true;  
            }
        }
    }
        
    for(i = 0; i < sequence.size() - 1; i++) {
        for(std::vector<int>::const_iterator i = sequence.begin(); i != sequence.end(); i++)
        cout << *i << ' ';
        if(sequence[i] < sequence[i+1]) {
            if(i == sequence.size() - 1) {
                return true;
            }
        } else {
            return false;
        }
    }
}
*/

/*
        for(i = 0; i < sequence.size() - 1; i++) {
            if(sequence[i] < sequence[i+1]) {
                if(i == sequence.size() - 1) {
                    return true;
                }
            }
        }
        if(initialized) {
            if((sequence[i] > sequence[i+1]) || (sequence[i] == sequence[i+1])) {
                return false;
            }
        }
*/
        




