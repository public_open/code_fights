/*
Given an array of integers, find the maximal absolute difference between any two
of its adjacent elements.

Example

For inputArray = [2, 4, 1, 0], the output should be
arrayMaximalAdjacentDifference(inputArray) = 3.
*/

int arrayMaximalAdjacentDifference(std::vector<int> inputArray) {

    int difference = 0;
    int temp, temp1 = 0;
    int result = 0;
    
    for(int i = 0; i < inputArray.size() - 1; ++i) {

        int countLow = 0;
        int countHigh = 0;
        
        // index 0, greater than
        if(inputArray[i] > inputArray[i+1]) {
            temp = inputArray[i];
            while(temp > inputArray[i+1]) {
                temp--;
                countHigh++;
            }
        }
        // index 0, less than
        if(inputArray[i] < inputArray[i+1]) {
            temp1 = inputArray[i];
            while(temp1 < inputArray[i+1]) {
                temp1++;
                countLow++;
            }
        }

        if(countLow > countHigh) {
            difference = countLow;
        }
        if(countLow < countHigh) {
            difference = countHigh;
        }
        if(difference > result) {
            result = difference;
        }
        
        
        cout << "\nCH : " << countHigh << "\nCL : " << countLow << endl;
  
    }
    cout << "Result :: " << result << endl;
    return result;
    
}



