/*
An email address such as "John.Smith@example.com" is made up of a local part 
("John.Smith"), an "@" symbol, then a domain part ("example.com").

The domain name part of an email address may only consist of letters, digits, 
hyphens and dots. The local part, however, also allows a lot of different special 
characters. Here you can look at several examples of correct and incorrect email 
addresses.

Given a valid email address, find its domain part.

Example

For address = "prettyandsimple@example.com", the output should be
findEmailDomain(address) = "example.com";
For address = "<>[]:,;@\"!#$%&*+-/=?^_{}| ~.a\"@example.org", the output should be
findEmailDomain(address) = "example.org".
*/

std::string findEmailDomain(std::string address) {
    
    int found = 0;
    found = address.find_last_of(64);
    
    std::string result = "";
    for(int i = 0; i < address.size() - found - 1; i++) {
        result = result + address[found + i + 1];
    }
    for(auto x : result) 
    {
        cout << x;;
    }    
    return result;
}



