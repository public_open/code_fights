/*
Given a sorted array of integers a, find an integer x from a such that the 
value of

abs(a[0] - x) + abs(a[1] - x) + ... + abs(a[a.length - 1] - x)
is the smallest possible (here abs denotes the absolute value).
If there are several possible answers, output the smallest one.

Example

For a = [2, 4, 7], the output should be
absoluteValuesSumMinimization(a) = 4.
*/

int absoluteValuesSumMinimization(std::vector<int> a) {

    int sum[a.size()], min, min_number = 0;
    for(int i=0; i<a.size(); i++)
    {
        sum[i]=0;
        for(int j=0;j<a.size();j++)
        {
            sum[i]+=abs(a[j]-a[i]);
            //cout << sum[i] << " ";
        }
    }
    min=sum[0];
    //cout << "\n" << min << endl;
    min_number = a[0];
    for(int i=0;i<a.size();i++)
    {
        if(sum[i]<min)
        {
            min=sum[i];
            min_number=a[i];
        }
    }
    cout << "\nThe minimum sum is " << min_number << endl;
    
    return min_number;
}

