/*
Let's define digit degree of some positive integer as the number of times we 
need to replace this number with the sum of its digits until we get to a one 
digit number.

Given an integer, find its digit degree.

Example

For n = 5, the output should be
digitDegree(n) = 0;
For n = 100, the output should be
digitDegree(n) = 1.
1 + 0 + 0 = 1.
For n = 91, the output should be
digitDegree(n) = 2.
9 + 1 = 10 -> 1 + 0 = 1.
*/

int digitDegree(int n) {

    int temp = n;
    // length
    int len = 1;
    for (len = 0; temp > 0; len++) {
        temp = temp / 10;
    }
    cout << "len : " << len << endl;
    
    //1st iteration
    if(len == 1) {
        return 0;
    }
    
    std::vector<int> array;
    int firstNum = 0;
    // push into array
    for(int i = 0; i < len; i++) {
        firstNum = n % 10;
        array.push_back(firstNum);
        n = n / 10;
    }
    std::reverse(array.begin(), array.end());
    
    // summation check
    int sum = 0;
    for(int i = 1; i < len+1; i++) {
        sum = std::accumulate(array.begin(), array.end(), 0);
        if(sum <= 9) {
            return i;
        }
        array.erase(array.begin());
        cout << " " << sum;
    }
    
    for(auto x : array) {
        cout << " " << x;
    }

}


