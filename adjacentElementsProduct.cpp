/*
Given an array of integers, find the pair of adjacent elements that has the 
largest product and return that product.

Example

For inputArray = [3, 6, -2, -5, 7, 3], the output should be
adjacentElementsProduct(inputArray) = 21.

7 and 3 produce the largest product.
*/


#include <limits>

int adjacentElementsProduct(std::vector<int> inputArray) {
    
    int result;
    long highestNum = numeric_limits<long>::min();
    
    for(int i=0; i < inputArray.size() - 1; i++) {
        result = inputArray[i] * inputArray[i+1];
        if(result > highestNum) {
            highestNum = result;
        }
    }
    cout << "adjacentElementsProduct(inputArray) = " << highestNum;
    return highestNum;

}