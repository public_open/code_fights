bool checkPalindrome(std::string inputString) {

    int i = 0;
    int j;
    j = inputString.length() - 1;
    while(i < j)
    {
        if(inputString[i] != inputString[j])
        {
            return false;
        }
        i++;
        j--;
    }
    return true;
}

