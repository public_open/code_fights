/*
A string is said to be beautiful if b occurs in it no more times than a; c 
occurs in it no more times than b; etc.

Given a string, check whether it is beautiful.

Example

For inputString = "bbbaacdafe", the output should be
isBeautifulString(inputString) = true;
For inputString = "aabbb", the output should be
isBeautifulString(inputString) = false;
For inputString = "bbc", the output should be
isBeautifulString(inputString) = false.
*/

bool isBeautifulString(std::string inputString) {

    int alphabetNumCount[26] = {0};
    for(auto x : inputString) {
        int index = x - 'a';
        alphabetNumCount[index]++;
    }
    for(auto w : alphabetNumCount) {
        cout << " " << w;
    }
    cout << endl;
    for(int i = 0; i < 25; i++) {        
        if(alphabetNumCount[i] >= alphabetNumCount[i+1]) {
        } else {
            return false;
        }
    }
}



