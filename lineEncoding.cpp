/*
Given a string, return its encoding defined as follows:

First, the string is divided into the least possible number of disjoint 
substrings consisting of identical characters
for example, "aabbbc" is divided into ["aa", "bbb", "c"]
Next, each substring with length greater than one is replaced with a 
concatenation of its length and the repeating character
for example, substring "bbb" is replaced by "3b"
Finally, all the new strings are concatenated together in the same order and 
a new string is returned.

Example

For s = "aabbbc", the output should be
lineEncoding(s) = "2a3bc".
*/

std::string lineEncoding(std::string s) {

    int cnt = 1;
    std::vector<std::string> result;
    
    for(int i = 1; i < s.length(); i++) {
        
        if (s.at(i) == s.at(i - 1) && i != s.length() - 1) {
            cnt++;
        } 
        else if (s.at(i) == s.at(i - 1) && i == s.length() - 1)
        {
            cnt++;
            
            if (cnt > 1) 
            {
                result.push_back(std::to_string(cnt));
            }
            result.push_back(s.substr(i, 1));
        }
        else if (s.at(i) != s.at(i - 1) && i == s.length() - 1) 
        {
            if (cnt > 1)
            {
                result.push_back(std::to_string(cnt));
            }
            result.push_back(s.substr(i - 1, 1));
            result.push_back(s.substr(i , 1));
        }
        else
        {
            if (cnt > 1)
            {
                result.push_back(std::to_string(cnt));
            }
            result.push_back(s.substr(i - 1, 1));
            cnt = 1;
        }
    }
    
    std::string newString = "";
    for (auto x : result) {
        newString = newString + x;
    }
    return newString;
    
}