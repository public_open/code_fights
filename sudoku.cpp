/*
Sudoku is a number-placement puzzle. The objective is to fill a 9 × 9 grid with 
digits so that each column, each row, and each of the nine 3 × 3 sub-grids that 
compose the grid contains all of the digits from 1 to 9.

This algorithm should check if the given grid of numbers represents a correct 
solution to Sudoku.

Example

For the first example below, the output should be true. For the other grid, 
the output should be false: each of the nine 3 × 3 sub-grids should contain all 
of the digits from 1 to 9.
*/

// auto vector of vectors
// O(n^4), nested logic to make 3x3 square grid into a single vector
// COULD : clean and make seperate functions

bool sudoku(std::vector<std::vector<int>> grid) {

    // auto vector of vectors
/*
    for (auto &row: grid) {
        for (auto g : row) {
            std::cout << g << " ";
        }
        cout << endl;
    }
*/    
    std::vector<int> v;
    
    for(int k = 0; k < grid.size(); k+=3) {
        for(int l = 0; l < grid.size(); l+=3) {
            for (int i = k; i < 3+k; i++) {    
                for (int j = 0+l; j < 3+l; j++) {
                    v.push_back(grid[i][j]);
                }
            }
            
            std::sort(v.begin(), v.end());
            for (int i = 0; i < v.size()-1; i++) {
                if(v[i] == v[i+1] ) {
                    cout << v[i] << " " << v[i+1];
                    return false;
                }
            }
            v.clear();
        }
    }

    for(int i = 0; i < grid.size(); i++) {
        // by row
        for(int j = 0; j < grid.size(); j++) {
            v.push_back(grid[i][j]);
        }
        std::sort(v.begin(), v.end());
        for(int k = 0; k < v.size(); k++) {
            if(v[k] == v[k+1]) {
                return false;
            }
        }
        
        //for (auto x: v) {
          //  cout << x << " ";
        //}
        //cout << endl << endl;
        v.clear();
        
        // by col
        for(int j = 0; j < grid.size(); j++) {
            v.push_back(grid[j][i]);
        }
        std::sort(v.begin(), v.end());
        for(int k = 0; k < v.size(); k++) {
            if(v[k] == v[k+1]) {
                return false;
            }
        }
        cout << endl;
        v.clear();
    }
    
    
    return true;
}

//kinda cool
//moves diagonally
/*
    for(int k = 0; k < grid.size(); k+=3) {
        for (int i = k; i < 3+k; i++) {    
            for (int j = k; j < 3+k; j++) {
                v1.push_back(grid[i][j]);
            }
        }
*/
