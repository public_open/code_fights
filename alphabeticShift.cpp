/*
Given a string, replace each its character by the next one in the English alphabet 
(z would be replaced by a).

Example

For inputString = &quot;crazy&quot;, the output should be
alphabeticShift(inputString) = &quot;dsbaz&quot;.
*/

std::string alphabeticShift(std::string inputString) {

    for(auto &x: inputString) {
        if(x=='z') {
            x='a'; 
        } else {
            x++;
        }
    }
    return inputString;

}
    
/*    
    for(size_t i = 0; i < inputString.size(); i++) {
        
        if(inputString[i] == 'z') {
            inputString[i] = 'a';
        } else {
            inputString[i] = inputString[i] + 1;
        }
        
    }
    return inputString;
*/  
/*
    for(char &c: inputString)
        if(c=='z')
            c='a';
        else
            ++c;
    return inputString;
*/