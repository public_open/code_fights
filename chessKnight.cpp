/*
Given a position of a knight on the standard chessboard, find the number of 
different moves the knight can perform.

The knight can move to a square that is two squares horizontally and one square 
vertically, or two squares vertically and one square horizontally away from it. 
The complete move therefore looks like the letter L. Check out the image below 
to see all valid moves for a knight piece that is placed on one of the central 
squares.

Example

For cell = "a1", the output should be
chessKnight(cell) = 2.

For cell = "c2", the output should be
chessKnight(cell) = 6.
*/

 bool valid(int a, int b) {
    return a >= 0 && a < 8 && b >= 0 && b < 8;
}

int chessKnight(std::string cell) {
    int a = cell.front() - 'a', b = cell.back() - '1';
    cout << cell.front() - 'a' << endl;
    cout << cell.back() - '1' << endl;
    
    cout << abs(a) << " " << abs(b) << endl;
    
    return
        + valid(a - 2, b - 1)
        + valid(a - 2, b + 1)
        + valid(a + 2, b - 1)
        + valid(a + 2, b + 1)
        + valid(a - 1, b - 2)
        + valid(a - 1, b + 2)
        + valid(a + 1, b - 2)
        + valid(a + 1, b + 2)
    ;

}

