/*
Given an integer product, find the smallest positive (i.e. greater than 0) 
integer the product of whose digits is equal to product. If there is no such 
integer, return -1 instead.

Example

For product = 12, the output should be
digitsProduct(product) = 26;
For product = 19, the output should be
digitsProduct(product) = -1.
*/

int digitsProduct(int product) {

    int i, j=0;
    std::vector<int> result;
    int ans=0;
    
    if(product == 0) {
        return 10;
    }
    if(product < 10) {
        return product;
    }
    
    for(i=9; 1<i; i--) {
        
        while(product%i==0) {
            product = product/i;
            ans = i;
            result.push_back(ans);
        }
    }
    
    if(product > 10) {
        return -1;
    }

    std::reverse(result.begin(), result.end());
    for(auto x: result) {
        cout << x;
    }
    
    // nice way to convert from std::vector<int> to int // has to be int values
    ans=0;
    for(auto digit : result) {
        ans = ans * 10 + digit;
    }
    
    return ans;
    
// nice way to reverse order
    //for(i=j-1; i >= 0; i--) {
    //}
    
}

