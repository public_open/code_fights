/*
Given a rectangular matrix of characters, add a border of asterisks(*) to it.

Example

For

picture = [&quot;abc&quot;,
           &quot;ded&quot;]
the output should be

addBorder(picture) = [&quot;*****&quot;,
                      &quot;*abc*&quot;,
                      &quot;*ded*&quot;,
                      &quot;*****&quot;]
*/

#include <string>

std::vector<std::string> addBorder(std::vector<std::string> picture) {
    
    int length = picture[0].length();
    std::vector<std::string> result;
    std::string s;
    
    result.push_back(s.insert(0,length+2,'*'));
    for(int i = 0; i < picture.size(); i++) {
        picture[i] = '*' + picture[i] + '*';
    }
    
    for(int i = 0; i < picture.size(); i++) {
        result.push_back(picture[i]);
    }
    result.push_back(std::string(length+2, '*'));
    
    return result;
    
    /*
    for(auto y : picture) {
        cout << s;
        cout << y;
        cout << s;
        cout << endl;
    }
    */

}

int main() {
    
    std::vector<std::string> picture = {"abc","ded"};
    addBorder(picture);
}






