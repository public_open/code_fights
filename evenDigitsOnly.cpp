/*
Check if all digits of the given integer are even.

Example

For n = 248622, the output should be
evenDigitsOnly(n) = true;
For n = 642386, the output should be
evenDigitsOnly(n) = false.
*/

bool evenDigitsOnly(int n) {
    
    while(n > 0) {
        int currentDigit = n % 10;
        cout << currentDigit << " ";
        if(currentDigit % 2 != 0) {
            return false;
        }
        n /= 10;
    }
    
    return true;
}
 /* basic stack usage-- pulling from top
  * for std::vector 
     
    stack<int> digits;
    int sum = 0;
    while(n > 0) {
        digits.push(n % 10);
        sum += digits.top();
        n = n / 10;
    }
    
    while (digits.size() > 0) {
        cout << digits.top() << " ";
        digits.pop();
    }
    
    cout << endl << "Sum of digits is " << sum << endl;
 */

    /* trivial length
    int number_of_digits = 0;
    do {
        ++number_of_digits;
        n /= 10;        
    } while (n);
    */