
int centuryFromYear(int year) {
    
    if(year > 2000) {
        cout << "centuryFromYear(year) = 21"; 
        return 21;
    } else if((year >= 1901) && (year <= 2000))  {
        cout << "centuryFromYear(year) = 20";
        return 20;
    } else if((year >= 1801) && (year <= 1900)) {
        cout << "centuryFromYear(year) = 19";
        return 19;
    } else if((year >=1701) && (year <= 1800)) {
        cout << "centuryFromYear(year) = 18";
        return 18;
    } else if ((year >= 1601) && (year <= 1700)) {
        cout << "centuryFromYear(year) = 17";
        return 17;
    } else if ((year >= 1501) && (year <= 1600)) {
        cout << "centuryFromYear(year) = 16";
        return 16;
    } else if ((year >= 1401) && (year <= 1500)) {
        cout << "centuryFromYear(year) = 15";
        return 15;
    } else if ((year >= 1301) && (year <= 1400)) {
        cout << "centuryFromYear(year) = 14";
        return 14;
    } else if ((year >= 1201) && (year <= 1300)) {
        cout << "centuryFromYear(year) = 13";
        return 13;
    } else if ((year >= 1101) && (year <= 1200)) {
        cout << "centuryFromYear(year) = 12";
        return 12;
    } else if ((year >= 1001) && (year <= 1100)) {
        cout << "centuryFromYear(year) = 11";
        return 11;
    } else if ((year >= 901) && (year <= 1000)) {
        cout << "centuryFromYear(year) = 10";
        return 10;
    } else if ((year >= 801) && (year <= 900)) {
        cout << "centuryFromYear(year) = 9";
        return 9;
    } else if ((year >= 701) && (year <= 800)) {
        cout << "centuryFromYear(year) = 8";
        return 8;
    } else if ((year >= 601) && (year <= 700)) {
        cout << "centuryFromYear(year) = 7";
        return 7;
    } else if ((year >= 501) && (year <= 600)) {
        cout << "centuryFromYear(year) = 6";
        return 6;
    } else if ((year >= 401) && (year <= 500)) {
        cout << "centuryFromYear(year) = 5";
        return 5;
    } else if ((year >= 301) && (year <= 400)) {
        cout << "centuryFromYear(year) = 4";
        return 4;
    } else if ((year >= 201) && (year <= 300)) {
        cout << "centuryFromYear(year) = 3";
        return 3;
    } else if ((year >= 101) && (year <= 200)) {
        cout << "centuryFromYear(year) = 2";
        return 2;
    } else {
        return 1;
    }
    
}

