/*
Given two strings, find the number of common characters between them.

Example

For s1 = "aabcc" and s2 = "adcaa", the output should be
commonCharacterCount(s1, s2) = 3.

Strings have 3 common characters - 2 "a"s and 1 "c".
*/

int commonCharacterCount(std::string s1, std::string s2) {
    
    // map of character to int, iterate separate through s1 and s2
    int sOneLetters[26] = {0};
    int sTwoLetters[26] = {0};
    int lower[26] = {0};
    
    for(auto x : s1) { 
        int index = x - 'a';
        sOneLetters[index]++;
    }
    for(auto y : s2) {
        int index = y - 'a';
        sTwoLetters[index]++;
    }
    for(int i = 0; i < 26; i++) {
        lower[i] = std::min(sOneLetters[i], sTwoLetters[i]);
    }
    cout << " Common Occurences / No Mapping " << endl;
    for(auto w : lower) {
        cout << " " << w;
    }
    
    int i = 0;
    int sum = 0;
    cout << endl;
    for(auto z : lower) {
        if(z != 0) {
            cout << " " << char(i + 'a') << " occurs " << z << " times\n";
        }
        sum = z + sum;
        i++;
    }
    return sum;
    
    // can do without std::vector<int>?
    // int sum = std::accumulate(lower.begin(), lower.end(), int(0));
    
}




