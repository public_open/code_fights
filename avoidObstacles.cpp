/*
You are given an array of integers representing coordinates of obstacles 
situated on a straight line.

Assume that you are jumping from the point with coordinate 0 to the right. 
You are allowed only to make jumps of the same length represented by some integer.

Find the minimal length of the jump enough to avoid all the obstacles.

Example

For inputArray = [5, 3, 6, 7, 9], the output should be
avoidObstacles(inputArray) = 4.
*/


int avoidObstacles(std::vector<int> inputArray) {
    
    std::sort(inputArray.begin(), inputArray.end());
    unsigned int max = 0;
    
    for(unsigned int i = 0; i < inputArray.size(); i++) {
        if(inputArray[i] > max) {
            max = inputArray[i];
        }
    }
    /*
    unsigned int min = max;
    for(unsigned int i = 0; i < inputArray.size(); i++) {
        if(inputArray[i] < min) {
            min = inputArray[i];
        }
    }
    */
    int min = 1;
    bool indivisible = false;
    
    while(min <= max+1 && !indivisible) {
        
        cout << "min " << min << endl;
        indivisible = true;
        for(int j = 0; j < inputArray.size() && indivisible; j++) {
            cout << "IA[j] : " << inputArray[j] << endl;
            // if the next element value is divisible % == 0
            if(inputArray[j] % min == 0) {
                indivisible = false;
            }
        }
        min++;
    }
    cout << "MIN  : " << min;
    min--;
    
    return min;
}

//catch(std::out_of_range)    

/*
    while(true) {
        success = true;
        for(int i = 0; i <= inputArray.size(); i = i + jump) {
            if(inputArray[i] == std::string::npos) {
        }
    }
*/