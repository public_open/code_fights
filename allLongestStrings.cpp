/*
Given an array of strings, return another array containing all of its longest strings.

Example

For inputArray = ["aba", "aa", "ad", "vcd", "aba"], the output should be
allLongestStrings(inputArray) = ["aba", "vcd", "aba"].
*/

#include <vector>

std::vector<std::string> allLongestStrings(std::vector<std::string> inputArray) {

    std::vector<std::string> longArray;
    int longestLength  =  0;
    
    for(std::string string : inputArray) {
        if(string.size() > longestLength) {
            longestLength = string.size();
            longArray.clear();
        }
        if(string.size() == longestLength) {
            longArray.push_back(string);
        }
        for(std::string element : longArray) {
            cout << " " << element;
        }
        cout << endl;
    }
    return longArray;
}
*/
    std::vector<std::string> longArray;
    int temp = 0;
    
    cout << inputArray.size() << ": size of inputArray \n";
    for(int i = 0; i < inputArray.size(); i++) {
        if(inputArray[i].size() >= temp) {
            temp = inputArray[i].size();
        }
    }
    cout << temp;
    
    for(int i = 0; i < inputArray.size(); i++) {
        if(inputArray[i].size() == temp) {
            //cout <<  " " << inputArray[i];
            longArray.push_back(inputArray[i]);
            //cout << 'allLongestString(inputArray) = [' << longArray << ', ';
        }
    }
    for(std::string element : longArray) {
            cout << " " << element;
    }
    cout << endl;
    return longArray;

}


