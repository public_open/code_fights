/*
Define a word as a sequence of consecutive English letters. Find the longest 
word from the given string.

Example

For text = "Ready, steady, go!", the output should be
longestWord(text) = "steady".
*/


std::string longestWord(std::string text) {

    int length = text.length();
    //size_t comma = text.find_first_of(",");

    std::vector<string> words;
    string word = "";
    //(text[i] != ' ')

    for(int i = 0; i < text.length(); i++) {
        if((!isspace(text[i])) && isalpha(text[i])) {
            word += text[i];
        } else {
            if(word.length()>0) {
                words.push_back(word);
                word="";
            }
        }
    }
    
    if(word.length()>0) {
        words.push_back(word);
        word="";
    }
 /*   
    string answer = "";
    if(words.size() == 1) {
        answer = words[0];
        return answer;
    }
 */   
    cout << words.size() << endl;
    
    for(auto x : words) {
        cout << x;
    }
 
    string answer = words[0];
    for(int j = 0; j < words.size()-1; j++) {
        if(words[j].size() > words[j+1].size()) {
            answer = words[j];
        }
    }
    return answer;

}
/*
    //if (.find_first_of(" !@#$%^&*(){}:<>?~)`-=[];',./") != std::string::npos)
*/


