/*
Given a string, output its longest prefix which contains only digits.

Example

For inputString="123aa1", the output should be
longestDigitsPrefix(inputString) = "123".
*/

std::string longestDigitsPrefix(std::string inputString) {
    
    int count = 0;
    for(int i = 0; i < inputString.size(); i++) {
        if(isdigit(inputString[i])) {
            count++;
        }
    }
    if(count == inputString.size()) {
        return inputString;
    } 
    string result = "";
    
//    if(isdigit(inputString[0])) {
    
    size_t findFirstAlpha = 0;
    findFirstAlpha = inputString.find_first_of("abcdefghijklmnopqrstuvwxyz /!@#$%^&*()_+}{][]};':");
    cout << "find first : " << findFirstAlpha << endl;
      
    for(int i = 0; i < findFirstAlpha; i++) {
        result = result + inputString[i];
        cout << inputString[i] << " :i " << result[i] << " :res " << endl;
    }
    
    // cout << result.data() << endl;

    for(auto x : result) {
        cout << x;
    }

    return result;

  //  }
    
    return {};

}


