/*
Given some integer, find the maximal number you can obtain by deleting exactly one digit of the given number.

Example

For n = 152, the output should be
deleteDigit(n) = 52;
For n = 1001, the output should be
deleteDigit(n) = 101.
*/

int deleteDigit(int n) {
    
    int s;
    std::vector<int> digits;
    std::vector<int> temp;
    std::vector<int> tempTwo;
    
    int length = std::to_string(n).length();
    int tempLength = length;
  
     while (n != 0) {
         digits.push_back(n % 10);
         n /= 10;
    }
    std::reverse(digits.begin(), digits.end());
    
    temp = digits;
   
/*    
    s = 0;
    int answer = 0;
    for(int i = 0; i < digits.size(); i++) {
        for(int j = (int)digits.size() - 1; j >= 0; j--) {
            if (j != i) {
                cout << digits[i] << endl;
            }
        }
        answer = std::max(s, answer);
    }
    return answer;
*/   
    
    string myString = "";
    
    for(int i = 0; i < length; i++) {
        temp.erase(temp.begin()+i);
        
        for(int j = 0; j < temp.size(); j++) {
            myString += (temp[j] + '0');
        }
        //cout << myString << endl;
        
        int myInt = stoi(myString);
        tempTwo.push_back(myInt);
        
        myString.clear();
        temp = digits;
        
        cout << endl;
    }

    for(auto x: tempTwo) {
        cout << x << " ";
    }
    
    int answer = 0;
    for(int i = 0; i < tempTwo.size(); i++) {
        answer = std::max(tempTwo[i], answer);
    }
    cout << endl << answer;
    return answer;
}




